FROM openjdk:8-jdk
MAINTAINER Matthäus Mayer mc-hermit@mayer.pub

ADD https://www.feed-the-beast.com/projects/ftb-presents-hermitpack/files/2369307/download /root/hermitpack.zip
ADD https://minecraft.curseforge.com/projects/worldedit/files/2328371/download /root/worldedit-forge.jar
RUN unzip /root/hermitpack.zip -d /hermitpack && \
    rm /root/hermitpack.zip && \
    cp /root/worldedit-forge.jar /hermitpack/mods/worldedit-forge.jar && \
    rm /root/worldedit-forge.jar && \
    chmod +x /hermitpack/*.sh && \
    echo "eula=true" > /hermitpack/eula.txt && \
    mkdir /hermitpack/mc-config/ && \
    echo "[]" > /hermitpack/mc-config/ops.json && \
    echo "[]" > /hermitpack/mc-config/whitelist.json && \
    echo "[]" > /hermitpack/mc-config/banned-ips.json && \
    echo "[]" > /hermitpack/mc-config/banned-players.json && \
    ln -s /hermitpack/mc-config/ops.json /hermitpack/ops.json && \
    ln -s /hermitpack/mc-config/whitelist.json /hermitpack/whitelist.json && \
    ln -s /hermitpack/mc-config/banned-ips.json /hermitpack/banned-ips.json && \
    ln -s /hermitpack/mc-config/banned-players.json /hermitpack/banned-players.json && \
    apt-get update && \
    apt-get install -y --no-install-recommends tmux && \
    rm -rf /var/lib/apt/lists/*

VOLUME /hermitpack/world
VOLUME /hermitpack/backups
VOLUME /hermitpack/mc-config

EXPOSE 25565

CMD ["tmux", "new-session", "-s", "minecraft", "/bin/bash -l -c /hermitpack/ServerStart.sh" ]